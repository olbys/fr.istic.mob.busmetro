package fr.istic.mob.buskt;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by mac on 29/11/2019.
 */

public class SharedPreferencesHelper {

    private final  static String RECORDID = "recordid";

     static String getPersistedRECORDID(Context context, String defValue) {

        SharedPreferences pref = context.getSharedPreferences(RECORDID,MODE_PRIVATE);
        String url = pref.getString(RECORDID,defValue);
        return url;


    }

     static void persistRECORDID(Context context, String recordid) {

        SharedPreferences pref = context.getSharedPreferences(RECORDID,MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(RECORDID,recordid);
        editor.commit();
    }


}
