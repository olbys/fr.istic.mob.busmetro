package fr.istic.mob.buskt.database.dao;

import android.database.Cursor;

import java.util.List;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import fr.istic.mob.buskt.database.models_.Route;

@Dao
public interface RouteDao {

    @Query("SELECT * FROM Route WHERE route_id = :route_id")
    List<Route> getRoutes(long route_id);

    @Query("SELECT * FROM Route WHERE route_id = :route_id")
    Cursor getRoutesCursor(long route_id);


    @Query("SELECT * FROM Route")
    List<Route> getAllRoutes();

    @Query("SELECT * FROM Route")
    Cursor getAllRoutesCursor();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertRoute(Route route);

    @Update
    void updateRoute(Route route);

    @Query("DELETE FROM Route WHERE route_id = :route_id")
    void deleteRoute(long route_id);

    @Query("DELETE FROM Route")
    void deleteRouteAll();

    @Delete
    void delete(Route route);
}
