package fr.istic.mob.buskt;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;
import fr.istic.mob.buskt.Adapter.SpinRouteAdapter;
import fr.istic.mob.buskt.database.Repository;
import fr.istic.mob.buskt.services.ReadData;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class IntroActivity extends AppCompatActivity {

    private ViewPager screenPager;
    IntroViewPagerAdapter introViewPagerAdapter ;
    TabLayout tabIndicator;
    Button btnNext;
    int position = 0 ;
    Button btnGetStarted;
    Animation btnAnim ;
    TextView tvSkip;
    private int STORAGE_PERMISSION_CODE = 1;
    private final int ID_PROGRESSDIALOG = 1;
    ProgressDialog progressDialog;
    Repository repository;
    ProgressDialog pd;

    SpinRouteAdapter spinnerArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // make the activity on full screen

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        // when this activity is about to be launch we need to check if its openened before or not

        if (restorePrefData()) {

            Intent mainActivity = new Intent(getApplicationContext(),MainActivity.class );
            startActivity(mainActivity);
            finish();


        }

        setContentView(R.layout.activity_intro);

        // hide the action bar

        getSupportActionBar().hide();

        // ini views
        //btnNext = findViewById(R.id.btn_next);
        btnGetStarted = findViewById(R.id.btn_get_started);
        //tabIndicator = findViewById(R.id.tab_indicator);
        btnAnim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.button_animation);
        //tvSkip = findViewById(R.id.tv_skip);

        // fill list screen

        final List<ScreenItem> mList = new ArrayList<>();
       // mList.add(new ScreenItem("Fresh Food","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua, consectetur  consectetur adipiscing elit",R.drawable.img1));
       // mList.add(new ScreenItem("Fast Delivery","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua, consectetur  consectetur adipiscing elit",R.drawable.img2));
        mList.add(new ScreenItem("Easy Payment","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua, consectetur  consectetur adipiscing elit",R.drawable.img3));

        // setup viewpager
        //screenPager =findViewById(R.id.screen_viewpager);
        introViewPagerAdapter = new IntroViewPagerAdapter(this,mList);
        //screenPager.setAdapter(introViewPagerAdapter);

        // setup tablayout with viewpager

        //tabIndicator.setupWithViewPager(screenPager);

        // next button click Listner

//        btnNext.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {

//                position = screenPager.getCurrentItem();
//                if (position < mList.size()) {
//
//                    position++;
//                    screenPager.setCurrentItem(position);
//
//
//                }

//                if (position == mList.size()-1) { // when we reach to the last screen
//
//                    // TODO : show the GETSTARTED Button and hide the indicator and the next button
//
//                    loaddLastScreen();
//
//
//                }

//            }
//        });
//
//        // tablayout add change listener
//
//        tabIndicator.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//
//                if (tab.getPosition() == mList.size()-1) {
//
//                    loaddLastScreen();
//
//                }
//
//
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//
//            }
//        });

        // Get Started button click listener

        btnGetStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //open main activity

                Intent mainActivity = new Intent(getApplicationContext(),MainActivity.class);

                if(SharedPreferencesHelper.getPersistedRECORDID(getApplicationContext(),"notfound").equals("notfound")){

                    if (checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,android.os.Process.myPid(),android.os.Process.myUid()) == PackageManager.PERMISSION_GRANTED) {
                        //Permission is already granted
                        firstDownloadAndPersistJSON();
                    }
                    else {
                        //Requested permission and download first data if it is granted.
                        requestStoragePermission();
                    }
                }
                else{
                    Log.e(TAG, "onCreate: premier telechargement deja fait" );
                    startActivity(mainActivity);
                }

            }
        });


        // skip button click listener

//        tvSkip.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                screenPager.setCurrentItem(mList.size());
//            }
//        });



    }

    private boolean restorePrefData() {


        SharedPreferences pref = getApplicationContext().getSharedPreferences("myPrefs",MODE_PRIVATE);
        Boolean isIntroActivityOpnendBefore = pref.getBoolean("isIntroOpnend",false);
        return  isIntroActivityOpnendBefore;



    }

    private void savePrefsData() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("myPrefs",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("isIntroOpnend",true);
        editor.commit();


    }

    // show the GETSTARTED Button and hide the indicator and the next button
    private void loaddLastScreen() {

//        btnNext.setVisibility(View.INVISIBLE);
 //       btnGetStarted.setVisibility(View.VISIBLE);
//        tvSkip.setVisibility(View.INVISIBLE);
//        tabIndicator.setVisibility(View.INVISIBLE);
        // TODO : ADD an animation the getstarted button
        // setup animation
        btnGetStarted.setAnimation(btnAnim);

    }

    public void firstDownloadAndPersistJSON(){

        Log.e(TAG, "firstDownloadAndPersistJSON");

        OkHttpClient client = new OkHttpClient();

        String url = "https://data.explore.star.fr/explore/dataset/tco-busmetro-horaires-gtfs-versions-td/download/?format=json&timezone=Europe/Berlin";

        Request request = new Request.Builder()
                .url(url)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {

                    final String myResponse = response.body().string();

                    IntroActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                JSONArray jObj = new JSONArray(myResponse);
                                JSONObject last = (JSONObject) jObj.get(jObj.length()-1);
                                String recordid =(String)last.get("recordid");
                                Log.e(TAG, "onResponse: the recordid"+ recordid);
                                SharedPreferencesHelper.persistRECORDID(getApplicationContext(),recordid);

                                JSONObject fields = (JSONObject) last.get("fields");
                                String url =(String)fields.get("url");

                                new DownloadSilentFile().execute(url);


                            } catch (JSONException e) {
                                Log.e("JSON Parser", "Error parsing data " + e.toString());
                            }
                        }
                    });
                }
            }
        });

    }

    private void requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)) {

            new AlertDialog.Builder(this)
                    .setTitle("Permission needed")
                    .setMessage("This permission is needed because of this and that")
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(IntroActivity.this,
                                    new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
                        }
                    })
                    .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create().show();

        } else {
            ActivityCompat.requestPermissions(this,
                    new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_CODE)  {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission Accordée, super !", Toast.LENGTH_SHORT).show();

                firstDownloadAndPersistJSON();

            } else {
                Toast.makeText(this, "Permission refusée, consultez les paramètres pour l'attribuer et réessayer", Toast.LENGTH_SHORT).show();
            }
        }
    }

    class DownloadSilentFile extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... params) {
            int count;

            try {
                File folder = new File(Environment.getExternalStorageDirectory() + File.separator + "bus");
                boolean success = true;
                if (!folder.exists()) {
                    success = folder.mkdir();
                }
                if (success) {
                    Log.e(TAG, "doInBackground: dossier crée avec success");// Do something on success
                } else {
                    Log.e(TAG, "doInBackground: dossier non crée ");// Do something else on failure
                }
                URL url = new URL(params[0]);
                URLConnection connexion = url.openConnection();
                connexion.connect();

                int lengthofFile = connexion.getContentLength();
                Log.e(TAG, "doInBackground: la taille du fichier est " + lengthofFile);

                InputStream input = new BufferedInputStream(url.openStream());
                OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory() + File.separator + "bus" + File.separator + "star_data.zip");



                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / (lengthofFile)));
                    output.write(data, 0, count);
                }

                output.flush();
                output.close();
                input.close();


            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(ID_PROGRESSDIALOG);
        }

        @Override
        protected void onPostExecute(String s) {
            dismissDialog(ID_PROGRESSDIALOG);
            Log.e(TAG, "onPostExecute: Fin du premier telechargement " );
            new UnzipAndSave().execute();
        }
        @Override
        protected void onProgressUpdate(String... values) {
            Log.e(TAG, values[0]);
            progressDialog.setProgress(Integer.parseInt(values[0]));
        }


    }

    class UnzipAndSave extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... params) {

            Boolean isUnzipped = ZipHelper.unzip(Environment.getExternalStorageDirectory() + File.separator + "bus"+File.separator+"star_data.zip",
                    Environment.getExternalStorageDirectory() + File.separator + "bus");
            if (isUnzipped){
                Log.e(TAG, "doInBackground: unzip okay !");
            }
            else {
                Log.e(TAG, "doInBackground: unzip failed !");
            }

            try {
                ReadData readData = new ReadData(getApplicationContext());
                readData.loadCSVRoutes("routes.txt");
                readData.loadCSVStop("stops.txt");
                readData.loadCSVTrip("trips.txt");
                readData.loadCSVCalendar("calendar.txt");
                readData.loadCSVStopTime("stop_times.txt");
                boolean test = readData.updateDatabaseVersion();

                Log.e(" SAVE ->", " is :" + test);

            }catch (Exception ex){
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(2);
        }

        @Override
        protected void onPostExecute(String s) {
            Log.e(TAG, "onPostExecute: Fin du unzip and save " );
           dismissDialog(2);
            Intent mainActivity = new Intent(getApplicationContext(),MainActivity.class );
            savePrefsData();
            startActivity(mainActivity);
        }
        @Override
        protected void onProgressUpdate(String... values) {
            Log.e(TAG, values[0]);
        }


    }
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case ID_PROGRESSDIALOG:
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Telechargement en cours...");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progressDialog.setCancelable(false);
                progressDialog.show();
                return progressDialog;
            case 2:
                pd = new ProgressDialog(this);
                pd.setMessage("remplissage de la bd en cours");
                pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                pd.setCancelable(false);
                pd.setIndeterminate(true);
                pd.show();
                return pd;
            default:
                return null;

        }
    }
}
