package fr.istic.mob.buskt.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import fr.istic.mob.buskt.database.models_.Trip;

public class SpinTripAdapter extends ArrayAdapter<Trip> {

    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private List<Trip> trips;

    public SpinTripAdapter(@NonNull Context context, int resource, @NonNull List<Trip> trip) {
        super(context, resource, trip);
        this.context = context;
        trips = trip;
    }


    @Override
    public int getCount(){
        return trips.size();
    }

    @Override
    public Trip getItem(int position){
        return trips.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        label.setText(trips.get(position).toString());

        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }


}