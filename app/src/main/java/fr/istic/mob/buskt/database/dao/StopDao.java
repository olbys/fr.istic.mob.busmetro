package fr.istic.mob.buskt.database.dao;

import android.database.Cursor;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import fr.istic.mob.buskt.database.models_.Stop;

@Dao
public interface StopDao {

    @Query("SELECT * FROM Stop WHERE stop_id = :stop_id")
    List<Stop> getStops(long stop_id);

    @Query("SELECT * FROM Stop WHERE stop_id = :stop_id")
    Cursor getStopsCursor(long stop_id);

    @Query("SELECT * FROM Stop")
    List<Stop> getAllStops();

    @Query("SELECT * FROM Stop")
    Cursor getAllStopsCursor();


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertStop(Stop stop);

    @Update
    void updateStop(Stop route);

    @Query("DELETE FROM Stop WHERE stop_id = :stop_id")
    void deleteStop(long stop_id);

    @Delete
    void delete(Stop stop);

    @Query("DELETE FROM Stop")
    void deleteStopAll();
}
