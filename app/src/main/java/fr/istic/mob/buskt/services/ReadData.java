package fr.istic.mob.buskt.services;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.opencsv.CSVReader;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

import fr.istic.mob.buskt.database.AppDatabase;
import fr.istic.mob.buskt.database.dao.RouteDao;
import fr.istic.mob.buskt.database.dao.StopDao;
import fr.istic.mob.buskt.database.dao.TripDao;
import fr.istic.mob.buskt.database.models_.Calendar;
import fr.istic.mob.buskt.database.models_.Route;
import fr.istic.mob.buskt.database.models_.Stop;
import fr.istic.mob.buskt.database.models_.StopTime;
import fr.istic.mob.buskt.database.models_.Trip;

public class ReadData {

    Executor executor;
    private AppDatabase appDatabase;
    private Context appliContext;
    List <Route> routes;
    List<Trip> trips;
    List <Stop> stops;
    List <Calendar> calendars;
    List <StopTime> stopTimes;


    private static String SAMPLE_CSV_FILE_PATH = Environment.getExternalStorageDirectory() + File.separator + "bus"+File.separator;
    // valeur des fichiers csv


    public ReadData(Context context){
            routes = new ArrayList<>();
            trips = new ArrayList<>();
            stops = new ArrayList<>();
            calendars = new ArrayList<>();
            stopTimes = new ArrayList<>();
            this.appliContext =  context;
            appDatabase = AppDatabase.getInstance(appliContext);
            executor = appDatabase.getQueryExecutor();

    }


    public boolean updateDatabaseVersion(){

        // StopTime
        appDatabase.calendarDao().deleteCalendarAll();
        appDatabase.stopTimeDao().deleteStopTimeAll();
        appDatabase.stopDao().deleteStopAll();
        appDatabase.tripDao().deleteTripAll();
        appDatabase.routeDao().deleteRouteAll();
        try {
            executor.execute(new Runnable() {
                @Override
                public void run() {

                    try {

                        for (Route route : routes){
                            appDatabase.routeDao().insertRoute(route);
                        }

                        // Stop
                        for (Stop stop : stops){
                            appDatabase.stopDao().insertStop(stop);
                        }


                        for (Trip trip : trips){
                            appDatabase.tripDao().insertTrip(trip);
                        }

                        // StopTime
                        for (StopTime time : stopTimes){
                            appDatabase.stopTimeDao().insertStopTime(time);
                        }


                        for (Calendar cal : calendars){
                            appDatabase.calendarDao().insertCalendar(cal);
                        }
                    }catch ( Exception ex){
                        ex.printStackTrace();
                        Log.e(" une erreur ", " voici"+ ex.getMessage());

                    }
                    finally {
                        if( appDatabase !=  null)
                            appDatabase.close();
                    }

                }
            });

        }catch (Exception e){
            return false;

        }

        return true;
    }



    public void loadCSVRoutes(String path) {

        try {
            CSVReader csvReader =  new CSVReader(new FileReader(SAMPLE_CSV_FILE_PATH+path), ',');
            String[] record = null;
            int count = 0;
            while ((record = csvReader.readNext()) != null ){

                if(count !=0 ) {
                    Route route = new Route();
                route.setRoute_id(Long.parseLong(record[0]));
                route.setAgency_id(Long.parseLong(record[1]));
                route.setRoute_short_name(record[2]);
                route.setRoute_long_name(record[3]);
                route.setRoute_desc(record[4]);
                route.setRoute_type(record[5]);
                route.setRoute_url(record[6]);
                route.setRoute_color(record[7]);
                route.setRoute_text_color(record[8]);
                route.setRoute_sort_order(record[9]);
                    routes.add(route);
                }
                count++;
            }
            csvReader.close();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(" DATALOAD", "looading is baddddd  .....");
        }

    }


    public void loadCSVCalendar(String path) {

        try {
            CSVReader csvReader =  new CSVReader(new FileReader(SAMPLE_CSV_FILE_PATH+path), ',');
            String[] record = null;
            int count =0;
            while ((record = csvReader.readNext()) != null ){

                if(count !=0 ) {
                    Calendar cal = new Calendar();
                    cal.setService_id(Long.parseLong(record[0]));
                    cal.setMonday(Long.parseLong(record[1]));
                    cal.setTuesday(Long.parseLong(record[2]));
                    cal.setWednesday(Long.parseLong(record[3]));
                    cal.setThursday(Long.parseLong(record[4]));
                    cal.setFriday(Long.parseLong(record[5]));
                    cal.setSaturday(Long.parseLong(record[6]));
                    cal.setSunday(Long.parseLong(record[7]));
                    cal.setStart_date(record[8]);
                    cal.setEnd_date(record[9]);
                    calendars.add(cal);
                }
                count++;
            }
            csvReader.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(" DATALOAD", "looading is baddddd  .....");
        }
    }


    public void loadCSVStop(String path) {

        try {
            CSVReader csvReader =  new CSVReader(new FileReader(SAMPLE_CSV_FILE_PATH+path), ',');
            String[] record = null;
            int count = 0;
            while ((record = csvReader.readNext()) != null ){
                if(count !=0) {
                    Stop cal = new Stop();
                    cal.setStop_code(Long.parseLong(record[0]));
                    cal.setStop_name(record[1]);
                    cal.setStop_desc(record[2]);
                    cal.setStop_lat(Long.parseLong(record[3]));
                    cal.setStop_lon(Long.parseLong(record[4]));
                    cal.setZone_id(Long.parseLong(record[5]));
                    cal.setStop_url(record[6]);
                    cal.setLocation_type(record[7]);
                    cal.setParent_station(record[8]);
                    cal.setStop_timezone(record[9]);
                    cal.setWheelchair_boarding(Boolean.parseBoolean(record[10]));
                    stops.add(cal);
                }

            }
            csvReader.close();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(" DATALOAD", "looading is baddddd  .....");

        }

    }


    public void loadCSVTrip(String path) {


        try {
            CSVReader csvReader =  new CSVReader(new FileReader(SAMPLE_CSV_FILE_PATH+path), ',');
            String[] record = null;
            int count =0;
            while ((record = csvReader.readNext()) != null ){
                if(count !=0) {

                    Trip cal = new Trip();

                    cal.setRoute_id(Long.parseLong(record[0]));
                    cal.setService_id(Long.parseLong(record[1]));
                    cal.setTrip_id(Long.parseLong(record[2]));
                    cal.setTrip_headsign(record[3]);
                    cal.setTrip_short_name(record[4]);
                    cal.setDirection_id(Long.parseLong(record[5]));
                    cal.setBlock_id(record[6]);
                    cal.setShape_id(record[7]);
                    cal.setWheelchair_accessible(Boolean.parseBoolean(record[8]));
                    cal.setBikes_allowed(Boolean.parseBoolean(record[9]));
                    trips.add(cal);
                }
                count++;
            }
            csvReader.close();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(" DATALOAD", "looading is baddddd  .....");

        }

    }


    public void loadCSVStopTime(String path) {

        try {
            CSVReader csvReader =  new CSVReader(new FileReader(SAMPLE_CSV_FILE_PATH+path), ',');
            String[] record = null;
            int count = 0;
            while ((record = csvReader.readNext()) != null ){
                if(count !=0) {
                    StopTime cal = new StopTime();

                    cal.setTrip_id(Long.parseLong(record[0]));
                    cal.setArrival_time(record[1]);
                    cal.setDeparture_time(record[2]);
                    cal.setStop_id(Long.parseLong(record[3]));
                    cal.setStop_sequence(record[4]);
                    if(record[5].equals("")){
                        cal.setStop_headsign(0);
                    }else{ cal.setStop_headsign(Long.parseLong(record[5]));}

                    cal.setPickup_type(Long.parseLong(record[6]));
                    cal.setDrop_off_type(Long.parseLong(record[7]));
                    if(record[8].equals("")){
                        cal.setShape_dist_traveled(0);
                    } else {
                        cal.setShape_dist_traveled(Long.parseLong(record[8]));
                    }

                    stopTimes.add(cal);
                }
                count++;
            }
            csvReader.close();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(" DATALOAD", "looading is baddddd  .....");
        }

    }



    public List<Route> getRoutes() {
        return routes;
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public List<Stop> getStops() {
        return stops;
    }

    public List<Calendar> getCalendars() {
        return calendars;
    }

    public List<StopTime> getStopTimes() {
        return stopTimes;
    }
}
