package fr.istic.mob.buskt.database.dao;

import android.database.Cursor;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import fr.istic.mob.buskt.database.models_.StopTime;

@Dao
public interface StopTimeDao {


    @Query("SELECT * FROM StopTime WHERE stop_id = :stop_id")
    List<StopTime> getStopTimesByStop(long stop_id);

    @Query("SELECT * FROM StopTime WHERE stop_id = :stop_id")
    Cursor getStopTimesByStopCursor(long stop_id);

    @Query("SELECT * FROM StopTime WHERE trip_id = :trip_id")
    List<StopTime> getStopTimesByTrip(long trip_id);

    @Query("SELECT * FROM StopTime WHERE trip_id = :trip_id")
    Cursor getStopTimesByTripCursor(long trip_id);

    @Query("SELECT * FROM StopTime")
    List<StopTime> getAllStopTimes();

    @Query("SELECT * FROM StopTime")
    Cursor getAllStopTimesCursor();


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertStopTime(StopTime stopTime);

    @Update
    void updateStopTime(StopTime stopTime);

    @Query("DELETE FROM StopTime WHERE stop_id = :stop_id")
    void deleteStopTime(long stop_id);

    @Query("DELETE FROM StopTime WHERE trip_id = :trip_id")
    void deleteStopTimeByTrip(long trip_id);

    @Delete
    void delete(StopTime stopTime);

    @Query("DELETE FROM StopTime")
    void deleteStopTimeAll();
}
