package fr.istic.mob.buskt.database;

import android.content.Context;

import java.util.List;

import fr.istic.mob.buskt.database.dao.RouteDao;
import fr.istic.mob.buskt.database.dao.StopDao;
import fr.istic.mob.buskt.database.dao.TripDao;
import fr.istic.mob.buskt.database.models_.Route;
import fr.istic.mob.buskt.database.models_.Trip;

public class Repository {


    private RouteDao routeDao;
    private StopDao stopDao;
    private TripDao tripDao;
    private AppDatabase appDatabase;
    private Context appliContext;



    public Repository(Context context){
        appliContext = context;
        appDatabase = AppDatabase.getInstance(appliContext);
        routeDao = appDatabase.routeDao();
        stopDao = appDatabase.stopDao();
        tripDao = appDatabase.tripDao();
    }


    public List<Route> getRoutes(){
      return routeDao.getAllRoutes();
    }


    public List<Trip> getTripsByRoute_id( long route_id){
        return  tripDao.getAllTripsByroute(route_id);
    }
}
