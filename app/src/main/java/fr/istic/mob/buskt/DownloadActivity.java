package fr.istic.mob.buskt;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import androidx.appcompat.app.AppCompatActivity;
import fr.istic.mob.buskt.services.ReadData;

import static android.content.ContentValues.TAG;

public class DownloadActivity extends AppCompatActivity {

    private final int ID_PROGRESSDIALOG = 1;
    ProgressDialog progressDialog;
    ProgressDialog pd;
    Toast toast;
    Intent i ;
    String recordid;
    String path = Environment.getExternalStorageDirectory() + File.separator + "bus";
    String zipname = "star_data.zip";
    String progress;
    ReadData readData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_download);
        toast = Toast.makeText(this, R.string.downloadDone, Toast.LENGTH_SHORT);
         i = new Intent(this,MainActivity.class);
        Intent intent = getIntent();
        String url = intent.getStringExtra("url");
        recordid = intent.getStringExtra("recordid");

        StartDownload(url);

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case ID_PROGRESSDIALOG:
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Telechargement en cours...");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progressDialog.setCancelable(false);
                progressDialog.show();
                return progressDialog;
            case 2:
                pd = new ProgressDialog(this);
                pd.setMessage("remplissage de la bd en cours");
                pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                pd.setCancelable(false);
                pd.setIndeterminate(true);
                pd.show();
                return pd;
            default:
                return null;

        }
    }

    public void StartDownload(String url) {
        new DownloadActivity.DownloadFile().execute(url);
    }

    class DownloadFile extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... params) {
            int count;

            try {

                File folder = new File(path);
                boolean success = true;
                if (!folder.exists()) {
                    success = folder.mkdir();
                }
                if (success) {
                    Log.e(TAG, "doInBackground: fichier crée avec success");// Do something on success
                } else {
                    Log.e(TAG, "doInBackground: fichier crée avec echec");// Do something else on failure
                }
                URL url = new URL(params[0]);
                URLConnection connexion = url.openConnection();
                connexion.connect();

                int lengthofFile = connexion.getContentLength();
                Log.e(TAG, "doInBackground: la taille du fichier est " + lengthofFile);

                InputStream input = new BufferedInputStream(url.openStream());
                OutputStream output = new FileOutputStream(path+ File.separator + zipname);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / (lengthofFile)));
                    output.write(data, 0, count);
                }

                output.flush();
                output.close();
                input.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(ID_PROGRESSDIALOG);
        }

        @Override
        protected void onPostExecute(String s) {

        dismissDialog(ID_PROGRESSDIALOG);
        SharedPreferencesHelper.persistRECORDID(getApplicationContext(),recordid);
        Log.e(TAG, "onPostExecute: Fin du premier telechargement " );
        new UnzipAndSave().execute();


        }

        @Override
        protected void onProgressUpdate(String... values) {
            Log.e(TAG, values[0]);
            progressDialog.setProgress(Integer.parseInt(values[0]));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle bundle){
        super.onSaveInstanceState(bundle);

    }

    class UnzipAndSave extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... params) {

            Boolean isUnzipped = ZipHelper.unzip(Environment.getExternalStorageDirectory() + File.separator + "bus"+File.separator+"star_data.zip",
                    Environment.getExternalStorageDirectory() + File.separator + "bus");
            if (isUnzipped){
                Log.e(TAG, "doInBackground: unzip okay !");
            }
            else {
                Log.e(TAG, "doInBackground: unzip failed !");
            }

            try {
                ReadData readData = new ReadData(getApplicationContext());
                readData.loadCSVRoutes("routes.txt");
                readData.loadCSVStop("stops.txt");
                readData.loadCSVTrip("trips.txt");
                readData.loadCSVCalendar("calendar.txt");
                readData.loadCSVStopTime("stop_times.txt");
                boolean test = readData.updateDatabaseVersion();

                Log.e(" SAVE ->", " is :" + test);

            }catch (Exception ex){
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(2);
        }

        @Override
        protected void onPostExecute(String s) {
            Log.e(TAG, "onPostExecute: Fin du unzip and save " );
            dismissDialog(2);
            Intent mainActivity = new Intent(getApplicationContext(),MainActivity.class );
            startActivity(mainActivity);
        }
        @Override
        protected void onProgressUpdate(String... values) {
            Log.e(TAG, values[0]);
        }



    }
}
