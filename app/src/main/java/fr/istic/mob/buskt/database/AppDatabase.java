package fr.istic.mob.buskt.database;

import android.content.ContentValues;
import android.content.Context;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.OnConflictStrategy;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;
import fr.istic.mob.buskt.database.dao.CalendarDao;
import fr.istic.mob.buskt.database.dao.RouteDao;
import fr.istic.mob.buskt.database.dao.StopDao;
import fr.istic.mob.buskt.database.dao.StopTimeDao;
import fr.istic.mob.buskt.database.dao.TripDao;
import fr.istic.mob.buskt.database.models_.Calendar;
import fr.istic.mob.buskt.database.models_.Route;
import fr.istic.mob.buskt.database.models_.Stop;
import fr.istic.mob.buskt.database.models_.StopTime;
import fr.istic.mob.buskt.database.models_.Trip;


@Database(entities = {Route.class, Stop.class, Trip.class, StopTime.class, Calendar.class}
, version = 1 , exportSchema = false)

public abstract class AppDatabase extends RoomDatabase {



    // DAO pour les models
    public abstract RouteDao routeDao();
    public abstract StopDao  stopDao();
    public abstract TripDao  tripDao();
    public abstract StopTimeDao stopTimeDao();
    public abstract CalendarDao calendarDao();

    // Singleton
    private static volatile AppDatabase INSTANCE;
    private static final int NUMBER_Of_THREADS =4;
    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_Of_THREADS);


    /**
     *
     * @return Callback
     */
    private static Callback prepopulateDatebase(){
        return new Callback() {
            @Override
            public void onCreate(@NonNull SupportSQLiteDatabase db) {
                super.onCreate(db);

                ContentValues contentValues = new ContentValues();
                contentValues.put("route_id" , 1000);
                contentValues.put("agency_id" , 2);
                contentValues.put("route_short_name" , "QDB");
                contentValues.put("route_long_name" , "Du bonheur");
                db.insert("Route", OnConflictStrategy.IGNORE,contentValues);

            }
        };
    }

    /**
     *
     * @param context {@link Context}
     * @return AppDatabase
     */
    public static AppDatabase getInstance(Context context){
        if(INSTANCE ==null){
            synchronized (AppDatabase.class){
                if(INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "BusDatabase.db")
                            .allowMainThreadQueries()
                            .addCallback(prepopulateDatebase())
                            .build();
                }
            }
        }

        return INSTANCE;
    }




}
