package fr.istic.mob.buskt.database.models_;

import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity (indices = @Index("service_id"))
public class Calendar {

    @PrimaryKey private long service_id;

    private long monday;
    private long tuesday;
    private long wednesday;
    private long thursday;
    private long friday;
    private long saturday;
    private long sunday;
    private String start_date;
    private String end_date;

    /**
     * Constructeur par defaut
     */
    public Calendar(){}


//    /**
//     *
//     * @param service_id
//     */
//    public Calendar(long service_id){
//        this.service_id = service_id;
//    }

    // SETTERS AND GETTERS

    public long getService_id() {
        return service_id;
    }

    public void setService_id(long service_id) {
        this.service_id = service_id;
    }

    public long getMonday() {
        return monday;
    }

    public void setMonday(long monday) {
        this.monday = monday;
    }

    public long getTuesday() {
        return tuesday;
    }

    public void setTuesday(long tuesday) {
        this.tuesday = tuesday;
    }

    public long getWednesday() {
        return wednesday;
    }

    public void setWednesday(long wednesday) {
        this.wednesday = wednesday;
    }

    public long getThursday() {
        return thursday;
    }

    public void setThursday(long thursday) {
        this.thursday = thursday;
    }

    public long getFriday() {
        return friday;
    }

    public void setFriday(long friday) {
        this.friday = friday;
    }

    public long getSaturday() {
        return saturday;
    }

    public void setSaturday(long saturday) {
        this.saturday = saturday;
    }

    public long getSunday() {
        return sunday;
    }

    public void setSunday(long sunday) {
        this.sunday = sunday;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    // END SETTERS DANS GETTERS



}
