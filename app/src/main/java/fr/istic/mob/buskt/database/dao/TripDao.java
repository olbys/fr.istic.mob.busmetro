package fr.istic.mob.buskt.database.dao;

import android.database.Cursor;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import fr.istic.mob.buskt.database.models_.Trip;

@Dao
public interface TripDao {


    @Query(" SELECT * FROM Trip WHERE trip_id=:trip_id")
    List<Trip> getTrips(long trip_id);

    @Query(" SELECT * FROM Trip WHERE trip_id=:trip_id")
   Cursor getTripsCursor(long trip_id);

    @Query(" SELECT * FROM Trip")
    List<Trip> getAllTrips();

    @Query(" SELECT * FROM Trip")
    Cursor getAllTripsCursor();

    @Query("SELECT * FROM Trip WHERE Trip.route_id=:route_id")
    List<Trip> getAllTripsByroute(long route_id);


    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertTrip(Trip trip);

    @Update
    void updateTrip(Trip trip);

    @Query("DELETE FROM Trip WHERE trip_id = :trip_id")
    void deleteTrip(long trip_id);


    @Delete
    void delete(Trip trip);

    @Query("DELETE FROM Trip")
    void deleteTripAll();


}
