package fr.istic.mob.buskt.database.models_;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

//@Entity(foreignKeys = {
//    @ForeignKey(
//            entity = Trip.class,
//            parentColumns = "trip_id",
//            childColumns = "trip_id" ),
//    @ForeignKey(
//            entity = Stop.class,
//            parentColumns = "stop_id",
//            childColumns = "stop_id" )
//}, indices = @Index("stop_time_id"))
@Entity( indices = @Index("stop_time_id"))
public class StopTime {

   @PrimaryKey(autoGenerate = true)
   private long stop_time_id;
   private long trip_id;
   private long stop_id;

   private String arrival_time;
   private String departure_time;

   private String stop_sequence;
   private long stop_headsign;
   private long pickup_type;
   private long drop_off_type;
   private long shape_dist_traveled;

//    /**
//     * Constructeur par defauts
//     */
   public StopTime(){}


//    /**
//     *
//     * @param stop_id
//     * @param trip_id
//     */
//   public StopTime(long stop_id , long trip_id ){
//        this.stop_id = stop_id;
//        this.trip_id =  trip_id;
//   }

   // SETTERS AND GETTERS


    public long getStop_time_id() {
        return stop_time_id;
    }

    public void setStop_time_id(long stop_time_id) {
        this.stop_time_id = stop_time_id;
    }

    public long getTrip_id() {
        return trip_id;
    }

    public void setTrip_id(long trip_id) {
        this.trip_id = trip_id;
    }

    public long getStop_id() {
        return stop_id;
    }

    public void setStop_id(long stop_id) {
        this.stop_id = stop_id;
    }

    public String getArrival_time() {
        return arrival_time;
    }

    public void setArrival_time(String arrival_time) {
        this.arrival_time = arrival_time;
    }

    public String getDeparture_time() {
        return departure_time;
    }

    public void setDeparture_time(String departure_time) {
        this.departure_time = departure_time;
    }

    public String getStop_sequence() {
        return stop_sequence;
    }

    public void setStop_sequence(String stop_sequence) {
        this.stop_sequence = stop_sequence;
    }

    public long getStop_headsign() {
        return stop_headsign;
    }

    public void setStop_headsign(long stop_headsign) {
        this.stop_headsign = stop_headsign;
    }

    public long getPickup_type() {
        return pickup_type;
    }

    public void setPickup_type(long pickup_type) {
        this.pickup_type = pickup_type;
    }

    public long getDrop_off_type() {
        return drop_off_type;
    }

    public void setDrop_off_type(long drop_off_type) {
        this.drop_off_type = drop_off_type;
    }

    public long getShape_dist_traveled() {
        return shape_dist_traveled;
    }

    public void setShape_dist_traveled(long shape_dist_traveled) {
        this.shape_dist_traveled = shape_dist_traveled;
    }


    // END SETTERS

}
