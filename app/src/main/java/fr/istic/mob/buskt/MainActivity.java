package fr.istic.mob.buskt;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.textview.MaterialAutoCompleteTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import fr.istic.mob.buskt.Adapter.SpinRouteAdapter;
import fr.istic.mob.buskt.Adapter.SpinTripAdapter;
import fr.istic.mob.buskt.database.Repository;
import fr.istic.mob.buskt.database.models_.Route;
import fr.istic.mob.buskt.database.models_.Trip;
import fr.istic.mob.buskt.services.FetchDataTask;
import fr.istic.mob.buskt.services.ReadData;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static android.content.ContentValues.TAG;

public class MainActivity extends AppCompatActivity implements RadialTimePickerDialogFragment.OnTimeSetListener,
        CalendarDatePickerDialogFragment.OnDateSetListener  {

    public static Activity activityRef ;

    private static final String FRAG_TAG_DATE_PICKER = "fragment_date_picker_name";
    private static final String FRAG_TIME_PICKER = "timePickerDialogFragment";
    private static final String PATTERN_DATE = "EEEE, d MMMM yyyy HH:mm";

    //BottomNavigationView bottomNavigationView;
    Button btn_choose_horaire;
    Button btn_choose_date;
    TextView text_view_full_date;
    Calendar now ;
    SimpleDateFormat dateFormat;
    Spinner spinner;
    Repository repository;

    SpinRouteAdapter spinnerArrayAdapter;

    Spinner spinner_direction_bus;
    SpinTripAdapter spinner_directionAdapter;




    @Override
    protected void onCreate(Bundle savedInstanceState) {

        repository = new Repository(getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        activityRef = this ;

        //bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_menu) ;
        btn_choose_horaire = findViewById(R.id.btn_horaires);
        btn_choose_date = findViewById(R.id.btn_date);

        text_view_full_date =  findViewById(R.id.full_date_view);

        now = Calendar.getInstance();
        dateFormat = new SimpleDateFormat(PATTERN_DATE);
        text_view_full_date.setText(dateFormat.format(now.getTime()));


        btn_choose_horaire.setOnClickListener(onClickListenerHoraire);
        btn_choose_date.setOnClickListener(onClickListenerDate);


        // Spinner
        spinner = findViewById(R.id.spinner_bus);
        spinner_direction_bus = findViewById(R.id.spinner_direction);

        spinnerArrayAdapter= new SpinRouteAdapter(MainActivity.this,
                android.R.layout.simple_spinner_item,
                repository.getRoutes());
        spinner.setAdapter(spinnerArrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                int place = spinner.getSelectedItemPosition();
                Route route = (Route) spinner.getSelectedItem();

                List<Trip> trip_sens =  new ArrayList<>();
                for(Trip trp : repository.getTripsByRoute_id(route.getRoute_id()) ){

                     if(!iSameTripNAme(trip_sens, trp)){
                         trip_sens.add(trp);
                     }
                }

                for (Trip tp : trip_sens){
                    Log.e(" Tri[ ", " ->"+tp.getTrip_headsign());
                }
                Log.e(" R ", " ->"+route.getRoute_id()+ " position"+position);
                spinner_directionAdapter.clear();
                spinner_directionAdapter.addAll(trip_sens);
                spinner_directionAdapter.notifyDataSetChanged();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_direction_bus = findViewById(R.id.spinner_direction);
        spinner_directionAdapter = new SpinTripAdapter(MainActivity.this,
                R.layout.support_simple_spinner_dropdown_item, new ArrayList<Trip>());
        spinner_direction_bus.setAdapter(spinner_directionAdapter);
        spinner_directionAdapter.setNotifyOnChange(true);
        spinner_directionAdapter.notifyDataSetChanged();
        spinner_directionAdapter.notifyDataSetInvalidated();

//        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//                switch (item.getItemId()) {
//                    case R.id.action_horaire:
//                        Toast.makeText(MainActivity.this, "Horaire", Toast.LENGTH_SHORT).show();
//                        break;
//                    case R.id.action_home:
//                        Toast.makeText(MainActivity.this, "Home", Toast.LENGTH_SHORT).show();
//                        break;
//                    case R.id.action_setting:
//                        String api_test_json_url = "https://api.myjson.com/bins/hw802";
//                        new FetchDataTask(getApplicationContext()).execute(api_test_json_url);
//                        Toast.makeText(MainActivity.this, "Setting", Toast.LENGTH_SHORT).show();
//                        break;
//                }
//                return true;
//            }
//        });

        WorkManager mWorkManager = WorkManager.getInstance();
        PeriodicWorkRequest periodic_work = new PeriodicWorkRequest.Builder(CheckingWorker.class ,1, TimeUnit.MINUTES).build();
        mWorkManager.enqueue(periodic_work);

    }

    @Override
    public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {

        now = Calendar.getInstance();
        now.set(Calendar.HOUR, hourOfDay);
        now.set(Calendar.MINUTE, minute);
        text_view_full_date.setText(dateFormat.format(now.getTime()));
    }

    private boolean iSameTripNAme(List<Trip> trips, Trip data){
        if (trips.isEmpty()){
            trips.add(data);
        } else {
            for(Trip in : trips){
                if(in.getTrip_headsign().equals(data.getTrip_headsign())){
                    return true;
                }
            }
        }

        return false;
    }

    private View.OnClickListener onClickListenerHoraire = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            now = Calendar.getInstance();
            int hour = now.get(Calendar.HOUR_OF_DAY);
            int minute = now.get(Calendar.MINUTE);

            RadialTimePickerDialogFragment radial = new RadialTimePickerDialogFragment()
                    .setOnTimeSetListener(MainActivity.this)
                    .setStartTime(hour, minute)
                    .setDoneText(getString(R.string.text_ok))
                    .setCancelText(getString(R.string.text_cancel))
                    .setForced24hFormat()
                    .setThemeDark();
            radial.show(getSupportFragmentManager(), FRAG_TIME_PICKER);

            Toast.makeText(getApplicationContext() , " Bouttons clicke", Toast.LENGTH_LONG).show();

        }
    };


    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {

        now = Calendar.getInstance();
        now.set(Calendar.YEAR, year);
        now.set(Calendar.MONTH, monthOfYear);
        now.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        text_view_full_date.setText(dateFormat.format(now.getTime()));
    }

    private View.OnClickListener onClickListenerDate = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                    .setOnDateSetListener(MainActivity.this)
                    .setThemeDark();
            cdp.show(getSupportFragmentManager(), FRAG_TAG_DATE_PICKER);
        }
    };


    private AdapterView.OnItemSelectedListener onItemSelectedListenerBus = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            int place = spinner.getSelectedItemPosition();
            Route routes = (Route) spinner.getSelectedItem();
            Log.e(" DEBUF!", " position" + position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    public void onBackPressed() {

    }
}


