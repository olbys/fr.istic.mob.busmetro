package fr.istic.mob.buskt;

import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.content.ContentProvider;

import fr.istic.mob.buskt.database.AppDatabase;

/**
 * Created by mac on 12/01/2020.
 */

class StarProvider extends ContentProvider {


 private static final int QUERY_ALL_BUSROUTES = 1;
 private static final int QUERY_BUSROUTES_BY_ID = 2;

 private static final int QUERY_ALL_TRIPS = 3;
 private static final int QUERY_TRIPS_BY_ID = 4;

 private static final int QUERY_ALL_STOPS = 5;
 private static final int QUERY_STOPS_BY_ID = 6;

 private static final int QUERY_ALL_STOPTIMES = 7;
 private static final int QUERY_STOPTIMES_BY_ID = 8;

 private static final int QUERY_ALL_CALENDAR = 9;
 private static final int QUERY_CALENDAR_BY_ID = 10;

 private static final int QUERY_ALL_ROUTEDETAILS = 11;
 private static final int QUERY_ROUTEDETAILS_BY_ID = 12;




 private static final UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

 static {
         URI_MATCHER.addURI(StarContract.AUTHORITY, StarContract.BusRoutes.CONTENT_PATH, QUERY_ALL_BUSROUTES);
         URI_MATCHER.addURI(StarContract.AUTHORITY,StarContract.BusRoutes.CONTENT_PATH + "/#", QUERY_BUSROUTES_BY_ID);

         URI_MATCHER.addURI(StarContract.AUTHORITY, StarContract.Trips.CONTENT_PATH, QUERY_ALL_TRIPS);
         URI_MATCHER.addURI(StarContract.AUTHORITY,StarContract.Trips.CONTENT_PATH + "/#", QUERY_TRIPS_BY_ID);

         URI_MATCHER.addURI(StarContract.AUTHORITY, StarContract.Stops.CONTENT_PATH, QUERY_ALL_STOPS);
         URI_MATCHER.addURI(StarContract.AUTHORITY,StarContract.Stops.CONTENT_PATH + "/#", QUERY_STOPS_BY_ID);

         URI_MATCHER.addURI(StarContract.AUTHORITY, StarContract.StopTimes.CONTENT_PATH, QUERY_ALL_STOPTIMES);
         URI_MATCHER.addURI(StarContract.AUTHORITY,StarContract.StopTimes.CONTENT_PATH + "/#", QUERY_STOPTIMES_BY_ID);

         URI_MATCHER.addURI(StarContract.AUTHORITY, StarContract.Calendar.CONTENT_PATH, QUERY_ALL_CALENDAR);
         URI_MATCHER.addURI(StarContract.AUTHORITY,StarContract.Calendar.CONTENT_PATH + "/#", QUERY_CALENDAR_BY_ID);

         URI_MATCHER.addURI(StarContract.AUTHORITY, StarContract.RouteDetails.CONTENT_PATH, QUERY_ALL_ROUTEDETAILS);
         URI_MATCHER.addURI(StarContract.AUTHORITY,StarContract.RouteDetails.CONTENT_PATH + "/#", QUERY_ROUTEDETAILS_BY_ID);
         }

    @Override
    public boolean onCreate() {
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] strings, String s, String[] strings1, String s1) {

         AppDatabase appDatabase =  AppDatabase.getInstance(this.getContext());

        switch (URI_MATCHER.match(uri)) {
            case QUERY_ALL_BUSROUTES:
                return appDatabase.routeDao().getAllRoutesCursor();
            case QUERY_BUSROUTES_BY_ID:
                return appDatabase.routeDao().getRoutesCursor(Long.parseLong(uri.getLastPathSegment()));

            case QUERY_ALL_TRIPS:
                return appDatabase.tripDao().getAllTripsCursor();
            case QUERY_TRIPS_BY_ID:
                return appDatabase.tripDao().getTripsCursor(Long.parseLong(uri.getLastPathSegment()));

            case QUERY_ALL_STOPS:
                return appDatabase.stopDao().getAllStopsCursor();
            case QUERY_STOPS_BY_ID:
                return appDatabase.stopDao().getStopsCursor(Long.parseLong(uri.getLastPathSegment()));

            case QUERY_ALL_STOPTIMES:
                return appDatabase.stopTimeDao().getAllStopTimesCursor() ;
            case QUERY_STOPTIMES_BY_ID:
                return appDatabase.stopTimeDao().getStopTimesByStopCursor(Long.parseLong(uri.getLastPathSegment()));

            case QUERY_ALL_CALENDAR:
                return appDatabase.calendarDao().getAllCalendarsCursor();
            case QUERY_CALENDAR_BY_ID:
                return appDatabase.calendarDao().getCalendarCursor(Long.parseLong(uri.getLastPathSegment()));

            case QUERY_ALL_ROUTEDETAILS:
                //return appDatabase.;
            case QUERY_ROUTEDETAILS_BY_ID:
                // return StarContract.RouteDetails.CONTENT_ITEM_TYPE;

            default:
                return null;
        }
    }

    @Override
    public String getType(Uri uri) {


        switch (URI_MATCHER.match(uri)) {

            case QUERY_ALL_BUSROUTES:
                 return StarContract.BusRoutes.CONTENT_TYPE;

            case QUERY_BUSROUTES_BY_ID:
                 return StarContract.BusRoutes.CONTENT_ITEM_TYPE;

            case QUERY_ALL_TRIPS:
                return StarContract.Trips.CONTENT_TYPE;
            case QUERY_TRIPS_BY_ID:
                return StarContract.Trips.CONTENT_ITEM_TYPE;

            case QUERY_ALL_STOPS:
                return StarContract.Stops.CONTENT_TYPE;
            case QUERY_STOPS_BY_ID:
                return StarContract.Stops.CONTENT_ITEM_TYPE;

            case QUERY_ALL_STOPTIMES:
                return StarContract.StopTimes.CONTENT_TYPE;
            case QUERY_STOPTIMES_BY_ID:
                return StarContract.StopTimes.CONTENT_ITEM_TYPE;

            case QUERY_ALL_CALENDAR:
                return StarContract.Calendar.CONTENT_TYPE;
            case QUERY_CALENDAR_BY_ID:
                return StarContract.Calendar.CONTENT_ITEM_TYPE;

            case QUERY_ALL_ROUTEDETAILS:
                return StarContract.RouteDetails.CONTENT_TYPE;
            case QUERY_ROUTEDETAILS_BY_ID:
                return StarContract.RouteDetails.CONTENT_ITEM_TYPE;

             default: return null;
        }

    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;
    }
}
