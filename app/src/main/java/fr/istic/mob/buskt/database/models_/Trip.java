package fr.istic.mob.buskt.database.models_;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

//@Entity(foreignKeys = @ForeignKey(
//        entity = Route.class,
//        parentColumns = "route_id",
//        childColumns = "route_id"
//
//), indices = @Index("trip_id"))

@Entity(indices = @Index("trip_id"))
public class Trip {

    @PrimaryKey private long trip_id;

    private long route_id;
    private long service_id;
    private String trip_headsign;
    private String trip_short_name;
    private long direction_id;
    private String block_id;
    private String shape_id;
    private boolean wheelchair_accessible;
    private boolean bikes_allowed ;

    /**
     * Constructeur sans parametres
     */
    public Trip(){ }


//    /**
//     *
//     * @param route_id
//     * @param service_id
//     * @param trip_id
//     * @param trip_headsign
//     * @param trip_short_name
//     */
//    public Trip(long route_id , long service_id, long trip_id,
//    long trip_headsign , String trip_short_name){
//        this.route_id = route_id;
//        this.service_id = service_id;
//        this.trip_id = trip_id;
//        this.trip_headsign = trip_headsign;
//        this.trip_short_name =  trip_short_name;
//    }
//

    // SETTER AND GETTERS


    public long getRoute_id() {
        return route_id;
    }

    public void setRoute_id(long route_id) {
        this.route_id = route_id;
    }

    public long getService_id() {
        return service_id;
    }

    public void setService_id(long service_id) {
        this.service_id = service_id;
    }

    public long getTrip_id() {
        return trip_id;
    }

    public void setTrip_id(long trip_id) {
        this.trip_id = trip_id;
    }

    public String getTrip_headsign() {
        return trip_headsign;
    }

    public void setTrip_headsign(String trip_headsign) {
        this.trip_headsign = trip_headsign;
    }

    public String getTrip_short_name() {
        return trip_short_name;
    }

    public void setTrip_short_name(String trip_short_name) {
        this.trip_short_name = trip_short_name;
    }

    public long getDirection_id() {
        return direction_id;
    }

    public void setDirection_id(long direction_id) {
        this.direction_id = direction_id;
    }

    public String getBlock_id() {
        return block_id;
    }

    public void setBlock_id(String block_id) {
        this.block_id = block_id;
    }

    public String getShape_id() {
        return shape_id;
    }

    public void setShape_id(String shape_id) {
        this.shape_id = shape_id;
    }

    public boolean isWheelchair_accessible() {
        return wheelchair_accessible;
    }

    public void setWheelchair_accessible(boolean wheelchair_accessible) {
        this.wheelchair_accessible = wheelchair_accessible;
    }

    public boolean isBikes_allowed() {
        return bikes_allowed;
    }

    public void setBikes_allowed(boolean bikes_allowed) {
        this.bikes_allowed = bikes_allowed;
    }

    // END SETTER AND GETTERS


    public String toString(){
        return this.getTrip_headsign();
    }
}
