package fr.istic.mob.buskt.database.models_;

import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(indices = @Index("stop_id"))
public class Stop {

   @PrimaryKey private long stop_id;

   private long stop_code;
   private String stop_name;
   private String stop_desc;
   private long stop_lat;
   private long stop_lon;
   private long zone_id;
   private String stop_url;
   private String location_type;
   private String parent_station;
   private String stop_timezone;
   private boolean wheelchair_boarding;

    /**
     * Constructeur par defaut
     */
   public Stop(){}
//
//    /**
//     *
//     * @param stop_id
//     * @param stop_code
//     * @param stop_name
//     */
//   public Stop(long stop_id , long stop_code, String stop_name){
//       this.stop_id = stop_id;
//       this.stop_code = stop_code;
//       this.stop_name = stop_name;
//   }

   // SETTER AND GETTER

    public long getStop_id() {
        return stop_id;
    }

    public void setStop_id(long stop_id) {
        this.stop_id = stop_id;
    }

    public long getStop_code() {
        return stop_code;
    }

    public void setStop_code(long stop_code) {
        this.stop_code = stop_code;
    }

    public String getStop_name() {
        return stop_name;
    }

    public void setStop_name(String stop_name) {
        this.stop_name = stop_name;
    }

    public String getStop_desc() {
        return stop_desc;
    }

    public void setStop_desc(String stop_desc) {
        this.stop_desc = stop_desc;
    }

    public long getStop_lat() {
        return stop_lat;
    }

    public void setStop_lat(long stop_lat) {
        this.stop_lat = stop_lat;
    }

    public long getStop_lon() {
        return stop_lon;
    }

    public void setStop_lon(long stop_lon) {
        this.stop_lon = stop_lon;
    }

    public long getZone_id() {
        return zone_id;
    }

    public void setZone_id(long zone_id) {
        this.zone_id = zone_id;
    }

    public String getStop_url() {
        return stop_url;
    }

    public void setStop_url(String stop_url) {
        this.stop_url = stop_url;
    }

    public String getLocation_type() {
        return location_type;
    }

    public void setLocation_type(String location_type) {
        this.location_type = location_type;
    }

    public String getParent_station() {
        return parent_station;
    }

    public void setParent_station(String parent_station) {
        this.parent_station = parent_station;
    }

    public String getStop_timezone() {
        return stop_timezone;
    }

    public void setStop_timezone(String stop_timezone) {
        this.stop_timezone = stop_timezone;
    }

    public boolean isWheelchair_boarding() {
        return wheelchair_boarding;
    }

    public void setWheelchair_boarding(boolean wheelchair_boarding) {
        this.wheelchair_boarding = wheelchair_boarding;
    }


    // END SETTER AND GETTER



}
