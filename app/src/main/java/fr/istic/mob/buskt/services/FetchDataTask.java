package fr.istic.mob.buskt.services;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import androidx.room.Room;
import fr.istic.mob.buskt.database.AppDatabase;
import fr.istic.mob.buskt.database.dao.RouteDao;
import fr.istic.mob.buskt.database.dao.StopDao;
import fr.istic.mob.buskt.database.dao.TripDao;
import fr.istic.mob.buskt.database.models_.Route;
import fr.istic.mob.buskt.database.models_.Stop;
import fr.istic.mob.buskt.database.models_.Trip;

import static android.content.ContentValues.TAG;

public class FetchDataTask extends AsyncTask<String, Void, JSONObject> {


    private String url = "https://api.myjson.com/bins/hw802";

    private RouteDao routeDao;
    private StopDao stopDao;
    private TripDao tripDao;
    private AppDatabase appDatabase;
    private Context appliContext;

    ReadData readData;
    public FetchDataTask( Context context){
        appliContext = context;
        readData = new ReadData(this.appliContext);

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.e(" Service", " le service a ete lange");
    }

    @Override
    protected JSONObject doInBackground(String... strings) {
        Log.e(TAG, "onPostExecute: Fin du premier telechargement " );
        try {
            readData.loadCSVRoutes("routes.txt");
            readData.loadCSVStop("stops.txt");
            readData.loadCSVTrip("trips.txt");
            readData.loadCSVCalendar("calendar.txt");
            readData.loadCSVStopTime("stop_times.txt");
            boolean test = readData.updateDatabaseVersion();
            Log.e(" SAVE ->", " is :" + test);
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return null;
    }


    //
    @Override
    protected void onPostExecute(JSONObject response) {
        super.onPostExecute(response);
        boolean test = readData.updateDatabaseVersion();
        Log.e(" SAVE ->", " is :" + test);


    }

}

