package fr.istic.mob.buskt.database.dao;

import android.database.Cursor;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import fr.istic.mob.buskt.database.models_.Calendar;
import fr.istic.mob.buskt.database.models_.Trip;

@Dao
public interface CalendarDao {


    @Query(" SELECT * FROM Calendar WHERE service_id = :service_id")
    List<Calendar> getCalendar(long service_id);

    @Query(" SELECT * FROM Calendar WHERE service_id = :service_id")
    Cursor getCalendarCursor(long service_id);

    @Query(" SELECT * FROM Calendar ")
    List<Calendar> getAllCalendars();

    @Query(" SELECT * FROM Calendar ")
    Cursor getAllCalendarsCursor();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCalendar(Calendar calendar);

    @Update
    void updateCalendar(Calendar calendar);

    @Query("DELETE FROM Calendar WHERE service_id = :service_id")
    void deleteCalendar(long service_id);

    @Delete
    void delete(Calendar calendar);

    @Query("DELETE FROM Calendar")
    void deleteCalendarAll();

}
