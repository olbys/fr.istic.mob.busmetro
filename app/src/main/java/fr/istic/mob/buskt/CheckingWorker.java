package fr.istic.mob.buskt;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import android.os.Build;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import androidx.annotation.NonNull;
import androidx.work.ListenableWorker;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by mac on 30/11/2019.
 */

public class CheckingWorker extends Worker {

    private int ID_NOTIFICATION = 0;
    Notification notification;

    public CheckingWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public ListenableWorker.Result doWork() {

        Log.e(TAG, "doWork: " );

        CheckIfNewJSONData();

        return Result.SUCCESS;
    }

    public void CheckIfNewJSONData(){
        Log.e(TAG, "CheckIfNewJSONData: je debute" );
        OkHttpClient client = new OkHttpClient();

        String url = "https://data.explore.star.fr/explore/dataset/tco-busmetro-horaires-gtfs-versions-td/download/?format=json&timezone=Europe/Berlin";

        Request request = new Request.Builder()
                .url(url)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {

                    final String myResponse = response.body().string();

                   MainActivity.activityRef.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONArray jObj = new JSONArray(myResponse);

                                JSONObject last = (JSONObject) jObj.get(jObj.length()-1);

                                //recuperer le recordid contenu dans le json recu
                                String recordid =(String)last.get("recordid");

                                //recuperer le recordid contenu dans les SharedPreferences
                                String recordid_shared = SharedPreferencesHelper.getPersistedRECORDID(getApplicationContext(),"notfound");

                                if(!recordid.equals(recordid_shared) && !recordid_shared.equals("notfound")){
                                    Log.e(TAG, "run: il y a de nouveaux fichiers!" );

                                    JSONObject fields = (JSONObject) last.get("fields");
                                    String url =(String)fields.get("url");

                                    // lancer une notification qui permettra de faire le telechargement du fichier avec cette url

                                    // La notification est crée
                                    notification = new Notification();

                                    Intent notificationIntent = new Intent(getApplicationContext(), DownloadActivity.class);
                                    notificationIntent.putExtra("url",url);
                                    notificationIntent.putExtra("recordid",recordid);
                                    notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                    PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, 0);

                                    // Récupération du Notification Manager

                                    NotificationManager manager = (NotificationManager) MainActivity.activityRef.getSystemService(Context.NOTIFICATION_SERVICE);

                                    // Construction de la notification

                                    Notification.Builder builder = new Notification.Builder(getApplicationContext());
                                    builder.setAutoCancel(true);
                                    builder.setTicker("STAR Rennes Metropole ");
                                    builder.setContentTitle("Nouveaux fichiers disponibles !");
                                    builder.setContentText("Cliquez ici pour les télécharger");
                                    builder.setSmallIcon(R.drawable.ic_launcher_foreground);
                                    builder.setContentIntent(contentIntent);
                                    builder.setOngoing(true);
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                        builder.setSubText("Important");   //API level 16
                                    }
                                    builder.setNumber(100);
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                        builder.build();
                                    }

                                    notification = builder.getNotification();

                                    manager.notify(ID_NOTIFICATION, notification);

                                }
                                else{
                                    Log.e(TAG, "run: pas de nouveaux fichiers !" );
                                }


                            } catch (JSONException e) {
                                Log.e("JSON Parser", "Error parsing data " + e.toString());
                            }
                        }
                    });
                }
            }
        });
    }




}
