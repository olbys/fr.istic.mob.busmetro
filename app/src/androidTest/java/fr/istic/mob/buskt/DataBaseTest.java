package fr.istic.mob.buskt;

import android.content.Context;
import android.util.Log;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

import androidx.room.Database;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import fr.istic.mob.buskt.database.AppDatabase;
import fr.istic.mob.buskt.database.dao.RouteDao;
import fr.istic.mob.buskt.database.models_.Route;

@RunWith(AndroidJUnit4.class)
public class DataBaseTest {

    private RouteDao routeDao;
    private AppDatabase appDatabase;


    @Before
    public void createDB(){
        Context context = ApplicationProvider.getApplicationContext();
        appDatabase = Room.inMemoryDatabaseBuilder(context, AppDatabase.class).build();
        routeDao = appDatabase.routeDao();
    }

    @After
    public void closeDb() throws IOException {
        appDatabase.close();
    }

    @Test
    public void WriteTest() throws Exception{
        Route route = new Route();
        route.setRoute_id(2);
        route.setRoute_long_name("Ti test");
        routeDao.insertRoute(route);

        List<Route>  mesRoutes = routeDao.getRoutes(2);

        mesRoutes.forEach(new Consumer<Route>() {
            @Override
            public void accept(Route item) {
                Log.e(" DEBUG TEST", " " + item.getRoute_id());
                Log.e(" DEBUG TEST", " " + item.getRoute_long_name());
            }
        });

        assertFalse(" c'est vide", mesRoutes.isEmpty());




    }



}
